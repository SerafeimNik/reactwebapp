import React from 'react';

import './footer.styles.scss';


export const Footer =()=>(
    <div className="footer">
        <h2 className="footer__header"> &#169; Created By Serafeim Nikolaou </h2>
    </div>
);