import React from 'react';
import { signInWithGoogle } from'../../firebase/firebase.utils';
import FormInput from '../form-input/form-input.component';
import Button from '../button/button.component';
import  './sign-in.styles.scss';

 


///////////////////////////////////////////////////////////////////////////////////////////////////////////////
class SignIn extends React.Component{
    constructor(props){
        super(props);


        this.state= {
            email : '',
            password: ''
        }
    }

    handleSubmit = event =>{
        event.preventDefault();

        this.setState({email : '', password:''});
    }

    handleChange = event =>{
        const { value , name } = event.target;

        this.setState({ [name]: value });
    }

    render(){
        return(
            <div className="sign-in">
                <h2 className="title-si"> I ALREADY HAVE AN ACCOUNT</h2>
                <span className="comment-su">SIGN IN WITH EMAIL AND PASSWORD</span>

                <form onSubmit={this.handleSubmit}>
                    <FormInput name="email" type="email" handlechange={this.handleChange} label='email' value={this.state.email} required/>
                     <FormInput name="password" type ="password" handlechange={this.handleChange} label="password" value={this.state.password} required/>
                     <div className="buttons">
                     <Button type="submit"> Sign In  </Button>
                     <Button onClick={signInWithGoogle} isGoogleSignIN >
                         Sign up with Google 
                     </Button>
                     </div>
                </form>
            </div>
        )
    }
}

export default SignIn;