import React from 'react';
import './button.styles.scss';


const Button = ({ children , isGoogleSignIN, ...otherProps }) => (
    <button className={`${isGoogleSignIN ? 'google-sign-in' : ' ' } custom-button  signup-button`} 
    {...otherProps}>
        {children}
    </button>
)

export default Button;