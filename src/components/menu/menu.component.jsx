import React from 'react';

import MenuItem from '../menu-item/menu-item.component';

import './menu.styles.scss';


class Menu extends React.Component{
    constructor(){
        super()
        

        this.state ={
            sections: [
                {
                title: 'Youtube',
                imageUrl: 'https://source.unsplash.com/8LfE0Lywyak',
                description: 'Find your favorite songs',
                id: 1,
                linkUrl: 'youtube'},

                {
                    title: 'Weather',
                    imageUrl:'https://source.unsplash.com/DZ975EkUJH0',
                    description:'See the weather today',
                    id: 2,
                    linkUrl: 'weather'
                }
            ]
        }       
    }

    render(){
        return(
            <div className="menu">
          {
              this.state.sections.map(({ id , ...otherProps})=>(
                  <MenuItem key={id} {...otherProps}/>
              ))
          }
          </div>
        )
    }
}

export default Menu;