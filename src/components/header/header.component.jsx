import React from 'react';
import {auth} from '../../firebase/firebase.utils';
import { Link } from 'react-router-dom';
import './header.styles.scss';
import { ReactComponent as Logo } from '../../assets/logo.svg'


export const Header =({currentUser})=>(
    <div className="header">
       <Link className="header__logo" to='/'>
           <Logo className="logo"/>
       </Link>
       <div className="options">
           <Link className='option' to='/youtube'>
               Youtube
            </Link>
            <Link className='option' to='/weather'>
               Weather
            </Link>
            {
               currentUser ?
               <div className="option" onClick={()=>auth.signOut()}>SIGN OUT</div>
               :
               <Link className="option" to='/signin'>SIGN IN </Link>
            }
       </div>
    </div>
)

export default Header;