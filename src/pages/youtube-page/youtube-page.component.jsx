import React from 'react';
import './youtube-page.styles.scss';
import  Search  from '../../components/search-bar/search-bar.component';
import YoutubeVideos from '../../components/youtube-videos/youtube-videos.component';

const YoutubePage =()=>(
    <div className="youtube-page">
        <Search/>
        <YoutubeVideos/>
    </div>
    
);

export default YoutubePage;