import React from 'react';

import './signInSignUp.styles.scss';
import '../../components/sign-in/sign-in.component';
import SignIn from '../../components/sign-in/sign-in.component';
import SignUp from '../../components/sign-up/sign-up.component';


export const SignInAndSignUP =() =>(
    <div className="sign-in-and-sign-up">
        <SignIn/>
        <SignUp/>
    </div>
)

export default SignInAndSignUP;