import React from 'react';
import { Switch , Route} from 'react-router-dom';
import './App.css';
import HomePage from './pages/homepage/homepage.component';
import { Header } from '../src/components/header/header.component';
import {Footer} from './components/footer/footer.component';
import { SignInAndSignUP } from './pages/sign-in-and-sign-up/signInSignUp.component';
import { auth, createUserProfileDocument } from './firebase/firebase.utils';
import  YoutubePage from './pages/youtube-page/youtube-page.component';
import WeatherPage from './pages/weather-page/weather-page.component';

class App extends React.Component{
  constructor(){
    super()

    this.state={
      currentUser:null
    }    
  }


 unsubscribeFromAuth = null;



 componentDidMount(){
this.unsubscribeFromAuth = auth.onAuthStateChanged(async userAuth =>{
    if(userAuth){
      const userRef = await createUserProfileDocument(userAuth);

      userRef.onSnapshot(snapShot =>{
        this.setState({
          currentUser:{
            id : snapShot.id,
            ...snapShot.data()
          }
        });
        console.log(this.state);
      })

    }
    this.setState({currentUser: userAuth});
});
}

componentWillUnmount(){
    this.unsubscribeFromAuth();
  }


  render(){
    return(
    <div>
    <Header currentUser={this.state.currentUser}/>
      <Switch>
      <Route exact path= '/' component={HomePage}/>
      <Route exact path= '/youtube' component={YoutubePage}/>
      <Route exact path = '/signin' component={SignInAndSignUP}/>
      <Route exact path = '/weather' component={WeatherPage}/>
      </Switch>
   <Footer/>
    </div>
  
    )
  
  }

} 


export default App;
