import firebase from 'firebase/app';
import 'firebase/firebase-firestore' ;
import 'firebase/firebase-auth' ;
import { useRef } from 'react';

const config ={
    
        apiKey: "AIzaSyAF8w0tb2CbOZWXSbKYx6MIX5Ef1p0w1n8",
        authDomain: "project-db-4adce.firebaseapp.com",
        databaseURL: "https://project-db-4adce.firebaseio.com",
        projectId: "project-db-4adce",
        storageBucket: "project-db-4adce.appspot.com",
        messagingSenderId: "944078273307",
        appId: "1:944078273307:web:0967c69e6f985cd60d31be",
        measurementId: "G-QJZRZGF18X"
}


export const createUserProfileDocument = async (userAuth, additionalData ) =>{
        if (!userAuth) return;

        const userRef = firestore.doc(`users/${userAuth.uid}`);

        const snapShot = await userRef.get();

        if(!snapShot.exists){
                const { displayName , email} = userAuth;
                const createAt =new Date();

                try{
                    await userRef.set({
                            displayName,
                            email,
                            createAt,
                            ...additionalData
                    },)


                } catch (error){
                      console.log('error creating user ', error.massage);
                }
        }

        return userRef;
} 

firebase.initializeApp(config);


 export const auth =firebase.auth();
 export const firestore =firebase.firestore();

 const provider = new firebase.auth.GoogleAuthProvider();
 provider.setCustomParameters({prompt: 'select_account'});

 export const signInWithGoogle =() => auth.signInWithPopup(provider);
export default firebase;